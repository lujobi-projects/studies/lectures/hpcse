#include <vector>
#include <chrono>
#include <math.h>
#include <cstdio>
#include <stdio.h>
#include <stdlib.h>

std::vector<double> Ax_row( std::vector<double> &A, std::vector<double> &x ){

  size_t N = x.size();
  std::vector<double> y(N);

  for(size_t row = 0; row < N; row++){
    for(size_t col = 0; col < N; col++){
      y[row]+=A[row*N+col]*x[col];
    }
  }

  return y;
}



std::vector<double> Ax_col( std::vector<double> &A, std::vector<double> &x ){

  size_t N = x.size();
  std::vector<double> y(N);

  for(size_t row = 0; row < N; row++){
    for(size_t col = 0; col < N; col++){
      y[row]+=A[row*N+col]*x[col];
    }
  }

  return y;
}

/*
void print_vec(std::vector<double> input){
	for (size_t i = 0; i < input.size(); i++) {
		std::cout << input.at(i) << ' ';
	}
  std::cout << '\n';
}*/

double benchmark_Ax( std::vector<double> &A, std::vector<double> &x, bool row_major, double Ns){

  double times = 0;

  for( size_t i=0; i<Ns; i++){
    auto t1 = std::chrono::system_clock::now();
    if( row_major==true ){
      Ax_row(A, x);
    }
    else{
      Ax_col(A, x);
    }
    auto t2 = std::chrono::system_clock::now();
    times += std::chrono::duration<double>(t2-t1).count();
  }
  printf("Done in total %9.4fs  --  average %9.4fs\n", times, times/Ns);

  return times/Ns;
}



int main( int argc, char **argv )
{
  if( argc<3 ){
    printf("Usage: %s [N|matrix dimension] [Ns|number of iterations]\n",argv[0]);
    exit(0);
  }

  size_t N  = atoi(argv[1]) ;
  size_t Ns = atoi(argv[2]) ;
  std::vector<double> A(N*N), B(N*N), x(N);

  for(size_t i = 0; i < N; i++){
    for(size_t j = 0; j < N; j++){
      A[i*N+j]=j+i+2;
      B[j*N+i]=j+i+2;
    }
    x[i]=i+1;
  }

  printf("Working with matrix of dimension %zu\n",N);

  printf("A*x (row major).\n");
  double times1 = benchmark_Ax(A,x,true,Ns);

  printf("A*x (column major).\n");
  double times2 = benchmark_Ax(B,x,false,Ns);

  printf("-----------------\n");
  printf("Speedup %.8fs\n", times1/times2);


  return 0;
}
