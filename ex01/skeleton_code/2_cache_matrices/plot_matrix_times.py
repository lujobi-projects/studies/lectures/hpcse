import numpy as np
from  matplotlib import pyplot as plt

arr = np.loadtxt('matrix_matrix_times.txt')
names = ['unopt', 
'br_2', 
'br_4', 
'br_8', 
'br_16', 
'br_32', 
'br_64', 
'br_128', 
'bc2', 
'bc4', 
'bc8', 
'bc16', 
'bc32', 
'bc64', 
'bc128']

fig, ax = plt.subplots()
for i in range(1, len(arr[0])):
  ax.plot(arr[:, 0], arr[:, i], label=names[i-1])
ax.legend()
plt.savefig('matrix_times.png')