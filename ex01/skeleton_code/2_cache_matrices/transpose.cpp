#include <stdio.h>
#include <stdlib.h>
#include <vector>
#include <chrono>
#include <math.h>
#include <string>
/*
#include <iostream>
void print_vec(std::vector<double> input){

  size_t N = sqrt(input.size());
	for (size_t i = 0; i < input.size(); i++) {
    if (i%N == 0){
      std::cout << '\n';
    }
		std::cout << input.at(i) << ' ';
    
	}
  std::cout << '\n';
}*/

void transpose( std::vector<double> A ){

  size_t N = sqrt(A.size());
  std::vector<double> AT(N*N);
  for(size_t row = 0; row < N; row++){
    for(size_t col = 0; col < N; col++){
      AT[row*N + col] = A[row + col*N];
    }
  }
}


void transpose_block( std::vector<double> A, size_t blockSize ){
  size_t N = sqrt(A.size());
  std::vector<double> AT(N*N);
  for(size_t row = 0; row < N; row+=blockSize){
    for(size_t col = 0; col < N; col+=blockSize){
      for(size_t block_row = 0; block_row < blockSize; block_row++){
        for(size_t block_col = 0; block_col < blockSize; block_col++){
          //printf("Row %d, Col %d\n", (row+block_row), col+block_col);
          AT[(row+block_row)*N + col+block_col] = \
          A[row + block_row + (col+block_col) *N];
        }
      }
    }
  }
}


double benchmark_transpose( std::vector<double> A, size_t mode, size_t blockSize, size_t Ns ){

  size_t N = sqrt(A.size());
  double times = 0;

  // TODO: Check that the matrix size is divided by the blockSize when mode==2
  if( mode==2 &&  N%blockSize!=0 ){
    printf("Error: the size of the matrix (%zu) should be divided by the blockSize variable (%zu).\n",N,blockSize);
    exit(1);
  }

  for( size_t i=0; i<Ns; i++){
    auto t1 = std::chrono::system_clock::now();
    // TODO: Question 2b: Call the function to be benchmarked
    if( mode==1 ){
      transpose(A);
    }
    else if( mode==2 ){
      transpose_block(A, blockSize);
    }
    auto t2 = std::chrono::system_clock::now();
    times += std::chrono::duration<double>(t2-t1).count();
  }
  printf("Done in total %9.4fs  --  average %9.4fs\n", times, times/Ns);

  return times/Ns;

}


int main( )
{
  std::vector<int> matrixSize{ 1024, 2048, 4096 };
  size_t M = matrixSize.size();

  std::vector<size_t> blockSize{ 2, 4, 8, 16, 32, 64, 128 };
  size_t B = blockSize.size();

  size_t Ns = 2;

  std::vector<double> times1(M);
  std::vector< std::vector<double>> times2(B, std::vector<double>(M) );

  std::vector<double> A;

  // loop over matrix sizes
  for( size_t m=0; m<M; m++){

    printf("Working with a matrix of size %d\n",matrixSize[m]);

    size_t N = matrixSize[m];
    A = std::vector<double>(N*N);

    for(size_t i = 0; i < N; i++){
      for(size_t j = 0; j < N; j++){
        A[i*N+j]=i>=j ? 1 : 0;
      }
    }


    printf("Start transposing (non optimized).\n");
    times1[m] = benchmark_transpose( A, 1, 0, Ns );

    // loop over block sizes
    for( size_t b=0; b<B; b++){

      printf("Start transposing (optimized, block size=%zu).\n", blockSize[b] );
      times2[b][m] = benchmark_transpose( A, 2, blockSize[b], Ns );
    }

    printf("==================================================\n");
  }


  // write results to a file
  FILE *fp=nullptr;
  fp = fopen("transpose_times.txt","w");
  // write header to the file
  std::string header = "# N   time_unoptimized ";
  for(size_t b=0; b<B; b++)
    header = header + "  block_" + std::to_string(blockSize[b]);
  header = header + "\n";
  fprintf(fp,"%s",header.c_str());
  for(size_t m=0; m<M; m++){
    fprintf(fp,"%d %lf",matrixSize[m],times1[m]);
    for(size_t b=0; b<B; b++)
      fprintf(fp," %lf ",times2[b][m]);
    fprintf(fp,"\n");
  }
  fclose(fp);

  return 0;
}
