import numpy as np
from  matplotlib import pyplot as plt

arr = np.loadtxt('transpose_times.txt')
names = ['time_unoptimized', 
          'block_2',  
          'block_4',  
          'block_8',  
          'block_16',  
          'block_32',  
          'block_64',  
          'block_128']

fig, ax = plt.subplots()
for i in range(1, len(arr[0])):
  ax.plot(arr[:, 0], arr[:, i], label=names[i-1])
ax.legend()
plt.savefig('transpose_times.png')